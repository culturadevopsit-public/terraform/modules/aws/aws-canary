variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    name                         = string
    region                       = string
    blue_ami                     = string
    green_ami                    = string
    blue_user_data_file          = optional(string)
    green_user_data_file         = optional(string)
    lb_access_log_enabled        = optional(bool)
    lb_connection_log_enabled    = optional(bool)
    lb_delete_protection_enabled = optional(bool)
    alb_subnets_ids              = set(string)
    vpc_id                       = string
    traffic_distribution         = string
    blue_instance_type           = string
    green_instance_type          = string
    tags                         = optional(map(string))

  })
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
