output "alb_endpoint" {
  value = module.module_usage_howto.alb_endpoint
}

output "instances_ips" {
  value = module.module_usage_howto.instances_ips
}

output "debug" {
  value = module.module_usage_howto.debug
}
