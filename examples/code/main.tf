provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name                         = "basic_canary"
    lb_access_log_enabled        = true
    lb_connection_log_enabled    = true
    lb_delete_protection_enabled = false
    region                       = "eu-west-1"
    blue_ami                     = "ami-0c38b837cd80f13bb"
    green_ami                    = "ami-0c38b837cd80f13bb"
    blue_user_data_file          = "${path.cwd}/scripts/blue_user_data.sh"
    green_user_data_file         = "${path.cwd}/scripts/green_user_data.sh"
    blue_instance_type           = "t3.micro"
    green_instance_type          = "t3.micro"
    traffic_distribution         = "split"
    vpc_id                       = "vpc-784a8a1d"
    alb_subnets_ids              = toset(["subnet-4eaf5c17", "subnet-ef815e98"])

    tags = {
      Name = "basic_canary"
    }
  }
}
