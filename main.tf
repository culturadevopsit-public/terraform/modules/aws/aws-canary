
locals {
  resources = {
    name                         = var.resources.name
    vpc_id                       = var.resources.vpc_id
    region                       = var.resources.region
    blue_user_data_file          = try(var.resources.blue_user_data_file, null)
    green_user_data_file         = try(var.resources.green_user_data_file, null)
    lb_logs_bucket_name          = replace("${var.resources.name}-logs", "_", "-")
    lb_access_log_enabled        = try(var.resources.lb_access_log_enabled != null ? var.resources.lb_access_log_enabled : null, null)
    lb_connection_log_enabled    = try(var.resources.lb_connection_log_enabled != null ? var.resources.lb_connection_log_enabled : null, null)
    lb_delete_protection_enabled = try(var.resources.lb_delete_protection_enabled != null ? var.resources.lb_delete_protection_enabled : null, null)
    alb_subnets_ids              = toset(var.resources.alb_subnets_ids)
    traffic_distribution         = var.resources.traffic_distribution
    blue_ami                     = var.resources.blue_ami
    green_ami                    = var.resources.green_ami
    blue_instance_type           = var.resources.blue_instance_type
    green_instance_type          = var.resources.green_instance_type
    tags                         = try(var.resources.tags != null ? var.resources.tags : null, null)
  }

  traffic_dist_map = {
    blue = {
      blue  = 100
      green = 0
    }
    blue-90 = {
      blue  = 90
      green = 10
    }
    split = {
      blue  = 50
      green = 50
    }
    green-90 = {
      blue  = 10
      green = 90
    }
    green = {
      blue  = 0
      green = 100
    }
  }

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

provider "aws" {
  region = local.resources.region
}

resource "aws_s3_bucket" "logs" {
  bucket = local.resources.lb_logs_bucket_name

  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-alb"
  })
}

resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.logs.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "arn:aws:iam::156460612806:root"
        },
        "Action" : "s3:PutObject",
        "Resource" : "arn:aws:s3:::${aws_s3_bucket.logs.id}/AWSLogs/${data.aws_caller_identity.current.account_id}/*"
      }
    ]
  })
}

data "aws_caller_identity" "current" {}


resource "aws_security_group" "alb_sg" {
  name        = "${local.resources.name}-alb-sg"
  description = "Security group for ALB"
  vpc_id      = local.resources.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resources.name}-alb-sg"
  })
}

resource "aws_security_group" "instances_sg" {
  name        = "${local.resources.name}-instances-sg"
  description = "Security group for EC2 instances"
  vpc_id      = local.resources.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resources.name}-instances-sg"
  })
}

resource "aws_lb" "alb" {
  name                             = "alb"
  desync_mitigation_mode           = "defensive"
  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = try(var.resources.lb_delete_protection_enabled, true)
  enable_http2                     = true
  enable_waf_fail_open             = false
  idle_timeout                     = 60
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.alb_sg.id]
  subnets                          = local.resources.alb_subnets_ids

  dynamic "access_logs" {
    for_each = try(var.resources.lb_access_log_enabled, false) ? [1] : [0]
    content {
      bucket  = aws_s3_bucket.logs.id
      enabled = true
    }
  }

  dynamic "connection_logs" {
    for_each = try(var.resources.lb_connection_log_enabled, false) ? [1] : [0]
    content {
      bucket  = aws_s3_bucket.logs.id
      enabled = true
    }
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-alb"
  })
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"
    forward {
      target_group {
        arn    = aws_alb_target_group.blue.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "blue", 40)
      }

      target_group {
        arn    = aws_alb_target_group.green.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "green", 60)
      }

      stickiness {
        enabled  = true
        duration = 60
      }
    }
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-alb-listener"
  })
}

resource "aws_alb_target_group" "blue" {
  name                               = "tg-blue"
  connection_termination             = false
  deregistration_delay               = 300
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-tg-blue"
  })
}

resource "aws_alb_target_group" "green" {
  name                               = "tg-green"
  connection_termination             = false
  deregistration_delay               = 300
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-tg-green"
  })
}

resource "aws_instance" "blue_instance" {
  for_each                    = local.resources.alb_subnets_ids
  ami                         = local.resources.blue_ami
  instance_type               = local.resources.blue_instance_type
  subnet_id                   = each.value
  vpc_security_group_ids      = [aws_security_group.instances_sg.id]
  associate_public_ip_address = true

  user_data = local.resources.blue_user_data_file != null ? templatefile(local.resources.blue_user_data_file, {}) : null

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "BLUE_INSTANCE_${each.key} - ALB ${local.resources.name}"
  })
}

resource "aws_lb_target_group_attachment" "blue_instance_attach" {
  for_each         = local.resources.alb_subnets_ids
  target_group_arn = aws_alb_target_group.blue.arn
  target_id        = aws_instance.blue_instance[each.key].id
  port             = 80
}

resource "aws_instance" "green_instance" {
  for_each                    = local.resources.alb_subnets_ids
  ami                         = local.resources.green_ami
  instance_type               = local.resources.green_instance_type
  subnet_id                   = each.value
  vpc_security_group_ids      = [aws_security_group.instances_sg.id]
  associate_public_ip_address = true

  user_data = local.resources.green_user_data_file != null ? templatefile(local.resources.green_user_data_file, {}) : null

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "GREEN_INSTANCE_${each.key} - ALB ${local.resources.name}"
  })
}

resource "aws_lb_target_group_attachment" "green_instance_attach" {
  for_each         = local.resources.alb_subnets_ids
  target_group_arn = aws_alb_target_group.green.arn
  target_id        = aws_instance.green_instance[each.key].id
  port             = 80
}
